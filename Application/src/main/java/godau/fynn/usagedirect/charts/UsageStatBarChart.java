package godau.fynn.usagedirect.charts;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.*;
import androidx.fragment.app.Fragment;
import godau.fynn.usagedirect.R;
import godau.fynn.usagedirect.persistence.HistoryDatabase;
import im.dacer.androidcharts.bar.BarView;
import im.dacer.androidcharts.bar.Line;

public abstract class UsageStatBarChart extends Fragment {

    private TextView textView;
    protected BarView barView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);

        textView = view.findViewById(R.id.bar_chart_label);
        barView = view.findViewById(R.id.bar_chart);

        return view;
    }

    @Override
    @CallSuper
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setText(getText());

        new Thread(() -> {
            HistoryDatabase database = HistoryDatabase.get(getContext());

            getData(database);

            database.close();

            new Handler(Looper.getMainLooper()).post(this::onDataLoaded);
        }).start();

    }

    protected @LayoutRes int getLayout() {
        return R.layout.content_bar_view;
    }

    protected void setText(@StringRes int text) {
        textView.setText(text);
    }

    /**
     * Run outside of the main thread before {@link #onDataLoaded()} is run.
     * This is the time for the subclass to gather the data it needs from the
     * database. The database need not be closed here.
     */
    protected abstract void getData(HistoryDatabase database);

    protected abstract @StringRes int getText();

    /**
     * Responsible for displaying the data loaded from database in view.
     * Run on UI thread.
     */
    protected abstract void onDataLoaded();

    /**
     * Calculate positions of vertical lines and their texts for scale
     *
     * @param chartMax Maximum value (upper border) in the chart
     */
    protected void addScale(int chartMax) {
        // Calculate vertical line frequency
        int maxHours = (chartMax / 60 / 60) + 1;
        int frequency = 1;
        while (maxHours / frequency > 10) {
            // If a power of 10, increase by the factor 2
            if (Math.log10(frequency) % 1 == 0) {
                frequency *= 2;
            } else {
                // If last step was a multiplication with 2, go back and multiply with 5
                if (Math.log10(frequency / 2) % 1 == 0) {
                    frequency = frequency / 2 * 5;
                } else {
                    // If last step was this multiplication with 5, go to next power of 10
                    frequency *= 2;
                }
            }
        }

        // Add lines
        // Note: maxHours is overestimating the total hours by up to one
        Line[] lines = new Line[(maxHours - 1) / frequency];

        for (int counter = frequency, i = 0; counter < maxHours; counter += frequency, i++) {
            lines[i] = new Line(counter * 60 * 60, String.valueOf(counter));
        }

        barView.setHorizontalLines(lines, chartMax);

        // Get window background
        TypedValue a = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.windowBackground, a, true);

        barView.setScrollHorizontalLines(
                // Is a color if this condition is true – don't scroll background otherwise
                a.type >= TypedValue.TYPE_FIRST_COLOR_INT && a.type <= TypedValue.TYPE_LAST_COLOR_INT,
                a.data
        );

    }
}
