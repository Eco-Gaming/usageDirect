package godau.fynn.usagedirect;

import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Dummy implementation in place of debug class
 */
public class DebugMenu {

    public static void addTo(Menu menu) {}

    public static boolean onOptionsItemSelected(MenuItem menuItem, Context context) {
        return false;
    }
}
