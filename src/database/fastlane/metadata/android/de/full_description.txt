– Zeigt verlässliche Nutzungsstatistiken bis zu zehn Tage vor Installation der App
– Zeigt historische Daten in einem Graphen
– Stellt tägliche Nutzungszeiten in einem Uhr-Kuchendiagramm dar
– Zeigt durchschnittliche Nutzungszeit pro Wochentag
– Widget mit der Nutzungszeit des Tages
– Weitere tolle Ideen für zukünftige Versionen!

Alle verfügbaren Statistiken werden für historische Daten in einer App-Datenbank gespeichert.
